#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#define GPIO_DIR "/sys/class/gpio"
#define GPIO_PIN 10
#define GPIO_BTN 3

int set_gpio_val(int gpio_pin, int value)
{
    char buf[64] = {0};
    snprintf(buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio_pin);
    int fd = open(buf, O_WRONLY);
    if(fd < 0){
        char buf2[100] = {"can't open "};
	memcpy(buf2+strlen(buf2), buf, strlen(buf));
	perror(buf2);
	close(fd);
	return 1;
    }
    if(value)
        write(fd, "1", 2);
    else
        write(fd, "0", 2);
    close(fd);
    return 0;
}

int set_gpio_export(int gpio_pin)
{
        int fd = open(GPIO_DIR "/export", O_WRONLY);
            if(fd < 0)
    {
        char buf2[] = {"can't open " GPIO_DIR "/export"};
        //memcpy(buf2+strlen(buf2), buf, strlen(buf));
        perror(buf2);
        return 1;
    }
        char buf[64] = {0};
        int len = snprintf(buf, sizeof(buf), "%d", gpio_pin);
        write(fd, buf, len);
        close(fd);
        return 0;
}

int set_gpio_direction(int gpio_pin, int direction){
char buf[64] = {0};
int len = snprintf(buf, sizeof(buf), GPIO_DIR "/gpio%d/direction", gpio_pin);
int fd = open(buf, O_WRONLY);
if(fd < 0){
perror("gpio/dir err");
return 1;
}
if(direction == 0)
write(fd, "out", sizeof("out"));
else
write(fd, "in", sizeof("in"));

close(fd);
return 0;
}

static int set_gpio_edge(int gpio_pin, char *edge){
int fd;
char buf[100] = {0};
snprintf(buf, sizeof(buf), GPIO_DIR "/gpio%d/edge", gpio_pin);
fd = open(buf, O_WRONLY);
if(fd < 0){
perror("gpio/edge\n\r");
return fd;
}
write(fd, edge, strlen(edge)+1);
close(fd);
return 0;
}

static int gpio_fd_open(int gpio_pin){
char buf[100] = {0};
snprintf(buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio_pin);
int fd = open(buf, O_RDONLY | O_NONBLOCK);
if(fd < 0)
perror("gpio/fdopen\r\n");
return fd;
}

int get_gpio_val(int gpio_pin){
int val = 0;
char buf[64] = {0};
snprintf(buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio_pin);
int fd = open(buf, O_RDONLY | O_NONBLOCK);
if(fd < 0){
perror("can't open btn val file");
return 0;
}
char value = 0;
read(fd, &value, 1);
if(value == '1')
val = 1;
else
val=0;
return val;
}

int main(){

set_gpio_export(GPIO_PIN);
set_gpio_direction(GPIO_PIN, 0);

set_gpio_export(GPIO_BTN);
set_gpio_direction(GPIO_BTN, 1);
//set_gpio_edge(GPIO_BTN, "falling");

while(1){
//int btn_val = get_gpio_val(GPIO_BTN);

//printf("Btn val = %d\n\r", btn_val);

set_gpio_val(GPIO_PIN, 0);
usleep(30000);
set_gpio_val(GPIO_PIN, 1);
usleep(30000);

}    
return 0;
}
