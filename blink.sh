#!/bin/sh
LED=10
LEDDIR=/sys/class/gpio/gpio$LED
if [ ! -d "$LEDDIR" ]; then
 echo "Exporting GPIO$LED"
 echo $LED > /sys/class/gpio/export
 echo "GPIO$LED already exported"
fi
echo out > $LEDDIR/direction
while true ; do
 echo 1 > $LEDDIR/value
 sleep 0.1
 echo 0 > $LEDDIR/value
 sleep 0.1
done
